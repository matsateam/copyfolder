package net.ukr.wumf;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {

		File folder1 = new File("folder1");
		folder1.mkdirs();
		File folder2 = new File("folder2");
		folder2.mkdirs();
		File abc = new File(folder1, "abc1.doc");
		try {
			abc.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		File abc2 = new File(folder1, "abc2.doc");
		try {
			abc2.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		FileFilter copyFolder = new SearchAndCopyFile(folder1, folder2);
		copyFolder.copyFile("doc");
	}
}
